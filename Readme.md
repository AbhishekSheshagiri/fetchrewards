![Fetch Rewards Logo](https://bitbucket.org/AbhishekSheshagiri/fetchrewards/raw/db8aebd4df4c8971de05bb025acef43a23a3e2a6/app/src/main/res/drawable/fetch.png)



# Fetch Rewards

Fetch Rewards is an Android application developed using Kotlin that shows various data provided by the Team using MVVM architecture and Fragments.

## Installation

Clone this repository using,

```
git clone https://AbhishekSheshagiri@bitbucket.org/AbhishekSheshagiri/fetchrewards.git
```

## Building

You can build the app with Android Studio or with `./gradlew assembleDebug` command.

## Screenshots

![Splash Screen](https://bitbucket.org/AbhishekSheshagiri/fetchrewards/raw/db8aebd4df4c8971de05bb025acef43a23a3e2a6/app/src/main/res/Screenshots/splash.png)

![Home Screen](https://bitbucket.org/AbhishekSheshagiri/fetchrewards/raw/db8aebd4df4c8971de05bb025acef43a23a3e2a6/app/src/main/res/Screenshots/home.png)

![Ignored Items Screen](https://bitbucket.org/AbhishekSheshagiri/fetchrewards/raw/db8aebd4df4c8971de05bb025acef43a23a3e2a6/app/src/main/res/Screenshots/ignored.png)

![Groups in Home Screen](https://bitbucket.org/AbhishekSheshagiri/fetchrewards/raw/db8aebd4df4c8971de05bb025acef43a23a3e2a6/app/src/main/res/Screenshots/groups.png)

![Items sorted by IDs](https://bitbucket.org/AbhishekSheshagiri/fetchrewards/raw/db8aebd4df4c8971de05bb025acef43a23a3e2a6/app/src/main/res/Screenshots/idlisted.png)

![Items sorted by Names](https://bitbucket.org/AbhishekSheshagiri/fetchrewards/raw/db8aebd4df4c8971de05bb025acef43a23a3e2a6/app/src/main/res/Screenshots/namelisted.png)

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.