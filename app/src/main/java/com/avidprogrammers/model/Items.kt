package com.avidprogrammers.model

interface Item {
    fun getItemType(): Int
}

const val PARENT = 0
const val PARENT_MENU = 3
const val CHILD = 1

data class Parent(val id: Long,val title: String) : Item {
    var parentMenu: ParentMenu? = null
    var childItems = ArrayList<Child>()
    var isExpanded = false
    var selectedChild: Child? = null
    override fun getItemType() = PARENT
}

data class Child(
    val parent: Parent,
    val id: Long,
    val title: String,
    var child_item_id: Int = 0) : Item {

    override fun getItemType() = CHILD
}
data class ParentMenu(val parent: Parent, val id: Int) : Item {

    val titleMenu: String= ""
    override fun getItemType() = PARENT_MENU
}
