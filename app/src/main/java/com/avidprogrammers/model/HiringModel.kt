package com.avidprogrammers.model

import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose

class HiringModel {
    @SerializedName("id")
    @Expose
    var id: Int? = null

    @SerializedName("listId")
    @Expose
    var listId: Int? = null

    @SerializedName("name")
    @Expose
    var name: String? = null
}