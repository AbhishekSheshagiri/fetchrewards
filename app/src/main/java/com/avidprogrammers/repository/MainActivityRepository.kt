package com.avidprogrammers.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.avidprogrammers.retrofit.RetrofitClient
import com.avidprogrammers.model.HiringModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object MainActivityRepository {

    val HiringMutableList = MutableLiveData<MutableList<HiringModel>>()

    fun getHiringApiCall(): MutableLiveData<MutableList<HiringModel>> {

        val call = RetrofitClient.apiInterface.getHiringApiData()

        call.enqueue(object : Callback<MutableList<HiringModel>> {
            override fun onFailure(call: Call<MutableList<HiringModel>>, t: Throwable) {
                Log.v("DEBUG : ", t.message.toString())
            }

            override fun onResponse(
                call: Call<MutableList<HiringModel>>,
                response: Response<MutableList<HiringModel>>
            ) {

                val data = response.body()

                HiringMutableList.value = data!!
            }
        })

        return HiringMutableList
    }
}