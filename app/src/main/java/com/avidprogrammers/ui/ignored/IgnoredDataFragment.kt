package com.avidprogrammers.ui.ignored

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.avidprogrammers.databinding.FragmentIgnoredDataBinding
import com.avidprogrammers.model.HiringModel
import com.avidprogrammers.ui.main.MainActivity
import com.avidprogrammers.viewmodel.IgnoredDataViewModel

class IgnoredDataFragment : Fragment() {

    private lateinit var dashboardViewModel: IgnoredDataViewModel
    private var _binding: FragmentIgnoredDataBinding? = null
    private var totalIgnoredItemList = mutableListOf<HiringModel>()

    private val binding get() = _binding!!

    private var mainActivity: MainActivity? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        dashboardViewModel =
            ViewModelProvider(
                this,
                ViewModelProvider.NewInstanceFactory()
            ).get(IgnoredDataViewModel::class.java)

        _binding = FragmentIgnoredDataBinding.inflate(inflater, container, false)
        val root: View = binding.root

        mainActivity = activity as MainActivity?



        dashboardViewModel.text.observe(viewLifecycleOwner, {
            binding.textDashboard.text = it
        })
        return root
    }

    fun updateUi() {
        mainActivity?.let { totalIgnoredItemList=it.totalIgnoredItemList }
        "Total ignored Items: ${totalIgnoredItemList.size}".also { binding.textDashboard.text = it }
    }

    private val mNotificationReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            updateUi()
        }
    }

    override fun onResume() {
        super.onResume()
        updateUi()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireActivity().registerReceiver(mNotificationReceiver, IntentFilter("update_IgnoredFrag"))
    }

    override fun onDestroyView() {
        super.onDestroyView()
        requireActivity().unregisterReceiver(mNotificationReceiver)
        _binding = null
    }

}