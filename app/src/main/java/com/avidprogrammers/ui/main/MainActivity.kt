package com.avidprogrammers.ui.main

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.avidprogrammers.R
import com.avidprogrammers.databinding.ActivityMainBinding
import com.avidprogrammers.model.HiringModel
import java.util.*
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.util.Log
import androidx.appcompat.app.AlertDialog
import com.avidprogrammers.model.Child
import com.avidprogrammers.model.Item
import com.avidprogrammers.model.Parent
import com.avidprogrammers.model.ParentMenu
import com.avidprogrammers.viewmodel.MainActivityViewModel


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    lateinit var mainActivityViewModel: MainActivityViewModel
    var hiringList = mutableListOf<HiringModel>()
    var totalIgnoredItemList = mutableListOf<HiringModel>()
    var hiringListSortById = mutableListOf<HiringModel>()
    var hiringListSortByName = mutableListOf<HiringModel>()

    var timelineObjMap: HashMap<String, MutableList<HiringModel>?> =
        HashMap<String, MutableList<HiringModel>?>()

    var headerList = mutableListOf<String>()
    val groupDataList = ArrayList<Item>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        mainActivityViewModel = ViewModelProvider(this).get(MainActivityViewModel::class.java)

        val navView: BottomNavigationView = binding.navView

        val navController = findNavController(R.id.nav_host_fragment_activity_main)

        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_home, R.id.navigation_dashboard
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        if(isOnline(this@MainActivity))
        {
            mainActivityViewModel.getHiring()!!.observe(this, Observer { dataList ->
                hiringList.clear()
                hiringListSortById.clear()
                hiringListSortByName.clear()

                hiringList.addAll(dataList)

                removeNulls(hiringList as MutableList<HiringModel>)

                if (totalIgnoredItemList.size>0) {
                    val intent = Intent("update_IgnoredFrag")
                    sendBroadcast(intent)
                }

                hiringListSortById.addAll(hiringList.sortedBy { HiringModel -> HiringModel.listId })
                groupDataByHash(hiringListSortById)
            })
        } else {
            showNetworkDialog(this@MainActivity)!!.show()
        }
    }

    fun groupDataByHash(list: MutableList<HiringModel>) {

        val tabsList = mutableListOf<String>()

        var _list: MutableList<HiringModel>? = null

        val map: HashMap<String, MutableList<HiringModel>?> =
            HashMap<String, MutableList<HiringModel>?>()

        for (i in list.indices) {
            val photo: HiringModel = list[i]

            var tabTitle = photo.listId.toString()

            if (tabsList.contains(tabTitle))
            {
                _list!!.add(photo)
                if (i == list.size - 1) {
                    val obj: MutableList<HiringModel>? = map[tabTitle]
                    if (obj != null && obj.size > 0) {
                        obj.addAll(_list)
                        map[tabTitle] = obj
                    } else map[tabTitle] = _list
                }
            } else
            {
                if (_list != null) {
                    map[tabsList[tabsList.size - 1]] = _list
                }
                _list = mutableListOf<HiringModel>()
                _list!!.clear()
                tabsList.add(tabTitle)
                _list.add(photo)
                if (i == list.size - 1) {
                    map[tabTitle] = _list
                }
            }
        }
        timelineObjMap = map
        headerList = tabsList

        groupData()
    }

    private fun groupData()
    {
        groupDataList.clear()
        for (i in headerList.indices) {
            val parent = Parent(i.toLong(), headerList[i])
            val childItems = ArrayList<Child>()

            val jdata = timelineObjMap[headerList[i]]

            for (j in jdata!!.indices) {
                childItems.add(
                    Child(
                        parent,
                        jdata[j].listId!!.toLong(),
                        jdata[j].name.toString(),
                        jdata[j].id!!
                    )
                )
                if (childItems.size == jdata.size) {
                    parent.childItems.clear()
                    childItems.sortBy  { it.child_item_id }
                    parent.childItems.addAll(childItems)
                    parent.parentMenu = ParentMenu(parent,i)
                    groupDataList.add(parent)
                    if (headerList.size == groupDataList.size) {
                        val intent = Intent("updateGroupFragment")
                        sendBroadcast(intent)
                    }
                }
            }
        }
    }

    private fun removeNulls(list: MutableList<HiringModel>) {
        val itr = list.iterator()
        totalIgnoredItemList.clear()
        while (itr.hasNext()) {
            val curr: HiringModel = itr.next()
            if (curr.name.isNullOrEmpty() ){
                // remove nulls
                totalIgnoredItemList.add(curr)
                itr.remove()
            }
        }
    }

    private fun showNetworkDialog(activity: Activity): AlertDialog.Builder? {
        val builder = AlertDialog.Builder(activity)
        builder.setTitle("No Internet Connection")
        builder.setMessage("You need to have Mobile Data or wifi to access this. Press ok to Exit")
        builder.setPositiveButton(
            "Ok"
        ) { dialogInterface, i -> activity.finish() }
        return builder
    }

    @SuppressLint("NewApi")
    fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val capabilities =
            connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
        if (capabilities != null) {
            when {
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                    return true
                }
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                    return true
                }
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                    return true
                }
            }
        }
        return false
    }
}