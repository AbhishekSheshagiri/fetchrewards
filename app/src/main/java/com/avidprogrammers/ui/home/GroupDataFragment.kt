package com.avidprogrammers.ui.home

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.avidprogrammers.databinding.FragmentGroupDataBinding
import com.avidprogrammers.ui.main.MainActivity
import com.avidprogrammers.viewmodel.GroupDataViewModel

class GroupDataFragment : Fragment() {

    private lateinit var homeViewModel: GroupDataViewModel
    private var _binding: FragmentGroupDataBinding? = null
    private val binding get() = _binding!!
    var mainActivity: MainActivity? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        homeViewModel =
            ViewModelProvider(
                this,
                ViewModelProvider.NewInstanceFactory()
            ).get(GroupDataViewModel::class.java)

        _binding = FragmentGroupDataBinding.inflate(inflater, container, false)
        val root: View = binding.root

        mainActivity = activity as MainActivity?

        val textView: TextView = binding.textHome


        homeViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.list.adapter = mainActivity?.let { GroupDataAdapter(it.groupDataList) }

    }

    fun updateUi() {
        binding.list.adapter!!.notifyDataSetChanged()
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private val mNotificationReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            updateUi()
        }
    }

    override fun onResume() {
        super.onResume()
        requireActivity().registerReceiver(mNotificationReceiver, IntentFilter("updateGroupFragment"))
    }

    override fun onPause() {
        super.onPause()
        requireActivity().unregisterReceiver(mNotificationReceiver)
    }
}