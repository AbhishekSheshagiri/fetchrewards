package com.avidprogrammers.ui.home

import android.animation.ArgbEvaluator
import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.OvalShape
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.avidprogrammers.R
import com.avidprogrammers.model.*
import com.avidprogrammers.uihelper.inflate

class GroupDataAdapter(private val itemList: ArrayList<Item>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var currentOpenedParent: Parent? = null
    private var currentOpenedParentMenu: ParentMenu? = null

    override fun getItemCount() = itemList.size

    override fun getItemViewType(position: Int): Int {
        return itemList[position].getItemType()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            CHILD -> ChildViewHolder(
                parent.inflate(
                    R.layout.item_child, false
                )
            )

            PARENT -> ParentViewHolder(parent.inflate(R.layout.new_item_parent, false))

            else -> ParentMenuViewHolder(
                parent.inflate(
                    R.layout.new_item_menu, false
                )
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder.itemViewType) {
            CHILD -> {
                val childViewHolder = (holder as ChildViewHolder)
                childViewHolder.childItem = itemList[position] as Child
                childViewHolder.bind()
            }
            PARENT -> {
                val parentViewHolder = holder as ParentViewHolder
                val parent = itemList[position]
                parentViewHolder.parentItem = parent as Parent
                parentViewHolder.parentMenuItem = parent.parentMenu as ParentMenu
                parentViewHolder.bind()
            }
            PARENT_MENU -> {
                val parentMenuViewHolder = holder as ParentMenuViewHolder
                parentMenuViewHolder.parentMenuItem = itemList[position] as ParentMenu
                parentMenuViewHolder.bind()
            }
        }
    }

    inner class ParentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.setOnClickListener {
                val startPosition = adapterPosition + 1
                val count = parentItem.childItems.size


                if (parentItem.isExpanded) {
                    itemList.remove(parentMenuItem)
                    itemList.removeAll(parentItem.childItems)
                    notifyItemRangeRemoved(startPosition, count+1)
                    parentItem.isExpanded = false
                    currentOpenedParent = null
                    currentOpenedParentMenu = null
                } else {
                    itemList.add(startPosition,parentMenuItem)
                    itemList.addAll(startPosition+1, parentItem.childItems)
                    notifyItemRangeInserted(startPosition, count+1)
                    parentItem.isExpanded = true

                    if (currentOpenedParent != null) {
                        itemList.remove(currentOpenedParentMenu as ParentMenu)
                        itemList.removeAll(currentOpenedParent!!.childItems)
                        notifyItemRangeRemoved(
                            itemList.indexOf(currentOpenedParent!!) + 1,
                            currentOpenedParent!!.childItems.size+1
                        )
                        currentOpenedParent?.isExpanded = false
                        notifyItemChanged(itemList.indexOf(currentOpenedParent!!))
                    }

                    currentOpenedParent = parentItem
                    currentOpenedParentMenu = parentMenuItem
                }
                updateViewState()
            }
        }

        lateinit var parentItem: Parent
        lateinit var parentMenuItem: ParentMenu

        private val title: TextView = itemView.findViewById(R.id.title)
        private val arrowImage: ImageView = itemView.findViewById(R.id.arrow_image)
        private val circleDrawable = CircleDrawable()


        fun bind() {
            arrowImage.rotation = if (parentItem.isExpanded) -180f else 0f
            arrowImage.background = circleDrawable
            circleDrawable.progress = if (parentItem.isExpanded) 1f else 0f
            updateViewState()
        }

        private fun updateViewState() {

            "Group By listId: ${parentItem.title}".also { title.text = it }

            arrowImage.animate()
                .setDuration(300)
                .rotation(if (parentItem.isExpanded) -180f else 0f)
                .setUpdateListener {
                    val progress =
                        if (parentItem.isExpanded) it.animatedFraction else 1 - it.animatedFraction
                    (arrowImage.background as CircleDrawable).progress = progress
                }
                .start()
        }
    }

    inner class ParentMenuViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        private val textListIdFilter: TextView = itemView.findViewById(R.id.txt_Id_filter)
        private val textListNameFilter: TextView = itemView.findViewById(R.id.txt_listName_filter)

        lateinit var parentMenuItem: ParentMenu
        fun bind() {
            textListIdFilter.setOnClickListener {

                val startPosition = adapterPosition + 1
                val count = parentMenuItem.parent.childItems.size

                parentMenuItem.parent.childItems.sortBy  { it.child_item_id }

                itemList.removeAll( parentMenuItem.parent.childItems)
                notifyItemRangeRemoved(startPosition, count+1)

                itemList.addAll(startPosition, parentMenuItem.parent.childItems)
                notifyItemRangeInserted(startPosition, count+1)
            }

            textListNameFilter.setOnClickListener {
                val startPosition = adapterPosition + 1
                val count = parentMenuItem.parent.childItems.size

                parentMenuItem.parent.childItems.sortBy  { it.title }

                itemList.removeAll( parentMenuItem.parent.childItems)
                notifyItemRangeRemoved(startPosition, count +1)

                itemList.addAll(startPosition, parentMenuItem.parent.childItems)
                notifyItemRangeInserted(startPosition, count+1)
            }
        }

    }

    inner class ChildViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        lateinit var childItem: Child

        private val textListId: TextView = itemView.findViewById(R.id.txt_listId)
        private val textListName: TextView = itemView.findViewById(R.id.txt_listName)

        fun bind() {
            textListId.text = childItem.child_item_id.toString()
            textListName.text = childItem.title
        }
    }

    private class CircleDrawable : ShapeDrawable(OvalShape()) {
        private val argbEvaluator = ArgbEvaluator()
        private val startColor = 0xff494949.toInt()
        private val endColor = 0xfff64637.toInt()
        var progress: Float = 0f
            set(value) {
                paint.color = argbEvaluator.evaluate(value, startColor, endColor) as Int
                invalidateSelf()
            }
    }
}
