package com.avidprogrammers.retrofit

import com.avidprogrammers.model.HiringModel
import retrofit2.Call
import retrofit2.http.GET

interface ApiInterface {
    @GET("hiring.json")
    fun getHiringApiData() : Call<MutableList<HiringModel>>
}