package com.avidprogrammers.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class IgnoredDataViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is ignored Fragment"
    }
    val text: LiveData<String> = _text
}