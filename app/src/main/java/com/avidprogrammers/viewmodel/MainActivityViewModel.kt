package com.avidprogrammers.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.avidprogrammers.repository.MainActivityRepository
import com.avidprogrammers.model.HiringModel

class MainActivityViewModel : ViewModel() {

    var servicesLiveData: MutableLiveData<MutableList<HiringModel>>? = null

    fun getHiring() : LiveData<MutableList<HiringModel>>? {
        servicesLiveData = MainActivityRepository.getHiringApiCall()
        return servicesLiveData
    }

}