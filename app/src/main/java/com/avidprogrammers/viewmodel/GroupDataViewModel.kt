package com.avidprogrammers.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class GroupDataViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is groupFragment"
    }
    val text: LiveData<String> = _text
}